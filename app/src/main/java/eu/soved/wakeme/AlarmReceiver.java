package eu.soved.wakeme;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.AlarmClock;
import android.provider.CalendarContract;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Calendar;

public class AlarmReceiver extends BroadcastReceiver {

    private static final String[] calendarProjection = new String[] {
            CalendarContract.Calendars._ID
    };

    private static final String[] eventProjection = new String[] {
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.TITLE
    };

    public AlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Helper.showToast("Alarm received...", context);

        Cursor calendars = context.getContentResolver().query(
                CalendarContract.Calendars.CONTENT_URI,
                calendarProjection,
                CalendarContract.Calendars.VISIBLE + " = 1",
                null,
                null
        );

        Calendar tonight = Calendar.getInstance();
        tonight.setTimeInMillis(System.currentTimeMillis());
        tonight.set(Calendar.HOUR_OF_DAY, 2);
        tonight.set(Calendar.MINUTE, 0);
        tonight.set(Calendar.SECOND, 0);
        tonight.set(Calendar.MILLISECOND, 0);

        Calendar afternoon = Calendar.getInstance();
        afternoon.setTimeInMillis(System.currentTimeMillis());
        afternoon.set(Calendar.HOUR_OF_DAY, 12);
        afternoon.set(Calendar.MINUTE, 0);
        afternoon.set(Calendar.SECOND, 0);
        afternoon.set(Calendar.MILLISECOND, 0);

        Calendar firstEventStart = null;
        String firstEventTitle = null;

        while (calendars.moveToNext()) {
            long calID = calendars.getLong(0);

            ArrayList<String> selection = new ArrayList<String>();
            selection.add(CalendarContract.Events.CALENDAR_ID + " = " + calID);
            selection.add(CalendarContract.Events.DTSTART + " > " + tonight.getTimeInMillis());

            if (firstEventStart != null) {
                selection.add(CalendarContract.Events.DTSTART + " < " + firstEventStart.getTimeInMillis());
            } else {
                selection.add(CalendarContract.Events.DTSTART + " < " + afternoon.getTimeInMillis());
            }

            Cursor events = context.getContentResolver().query(
                    CalendarContract.Events.CONTENT_URI,
                    eventProjection,
                    "(( " +  TextUtils.join(" AND ", selection) + " ))",
                    null,
                    CalendarContract.Events.DTSTART + " ASC"
            );

            if (events.moveToFirst()) {
                if (firstEventStart == null) {
                    firstEventStart = Calendar.getInstance();
                }

                firstEventStart.setTimeInMillis(Long.parseLong(events.getString(0)));
                firstEventTitle = events.getString(1);
            }
        }

        if (firstEventStart != null) {
            firstEventStart.add(Calendar.HOUR_OF_DAY, -2);

            Intent alarmClock = new Intent(AlarmClock.ACTION_SET_ALARM);
            alarmClock.putExtra(AlarmClock.EXTRA_HOUR, firstEventStart.get(Calendar.HOUR_OF_DAY));
            alarmClock.putExtra(AlarmClock.EXTRA_MINUTES, firstEventStart.get(Calendar.MINUTE));
            alarmClock.putExtra(AlarmClock.EXTRA_MESSAGE, firstEventTitle);
            alarmClock.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
            alarmClock.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(alarmClock);
        } else {
            Helper.showToast("No matching events found", context);
        }
    }

}
