package eu.soved.wakeme;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Scheduler scheduler = new Scheduler();
        scheduler.setRepeatingAlarm(this);
    }

}
