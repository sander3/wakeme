package eu.soved.wakeme;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Sander on 11/10/15.
 */
public class Helper {

    public static void showToast(CharSequence text, Context context) {
        int duration = Toast.LENGTH_SHORT;

        Toast.makeText(context, text, duration).show();
    }

}
